namespace Multiformanwendung
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmd_SubForm = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.textBox_Settings = new System.Windows.Forms.TextBox();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.txt_message = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_send = new System.Windows.Forms.TextBox();
            this.txt_partner = new System.Windows.Forms.TextBox();
            this.cmd_send_message = new System.Windows.Forms.Button();
            this.cmd_serial_start = new System.Windows.Forms.Button();
            this.cmd_serial_stop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmd_SubForm
            // 
            this.cmd_SubForm.Location = new System.Drawing.Point(488, 193);
            this.cmd_SubForm.Name = "cmd_SubForm";
            this.cmd_SubForm.Size = new System.Drawing.Size(97, 33);
            this.cmd_SubForm.TabIndex = 0;
            this.cmd_SubForm.Text = "Port-Settings";
            this.cmd_SubForm.UseVisualStyleBackColor = true;
            this.cmd_SubForm.Click += new System.EventHandler(this.cmd_SubForm_Click);
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(488, 250);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(97, 33);
            this.Exit.TabIndex = 1;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // textBox_Settings
            // 
            this.textBox_Settings.Location = new System.Drawing.Point(432, 309);
            this.textBox_Settings.Multiline = true;
            this.textBox_Settings.Name = "textBox_Settings";
            this.textBox_Settings.ReadOnly = true;
            this.textBox_Settings.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_Settings.Size = new System.Drawing.Size(153, 123);
            this.textBox_Settings.TabIndex = 2;
            // 
            // serialPort1
            // 
            this.serialPort1.PortName = "COM3";
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // txt_message
            // 
            this.txt_message.Location = new System.Drawing.Point(34, 38);
            this.txt_message.Name = "txt_message";
            this.txt_message.Size = new System.Drawing.Size(352, 22);
            this.txt_message.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Message:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Gesendet:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 267);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Partner:";
            // 
            // txt_send
            // 
            this.txt_send.Location = new System.Drawing.Point(37, 95);
            this.txt_send.Multiline = true;
            this.txt_send.Name = "txt_send";
            this.txt_send.Size = new System.Drawing.Size(349, 155);
            this.txt_send.TabIndex = 7;
            // 
            // txt_partner
            // 
            this.txt_partner.Location = new System.Drawing.Point(37, 286);
            this.txt_partner.Multiline = true;
            this.txt_partner.Name = "txt_partner";
            this.txt_partner.Size = new System.Drawing.Size(349, 146);
            this.txt_partner.TabIndex = 8;
            // 
            // cmd_send_message
            // 
            this.cmd_send_message.Location = new System.Drawing.Point(488, 38);
            this.cmd_send_message.Name = "cmd_send_message";
            this.cmd_send_message.Size = new System.Drawing.Size(97, 45);
            this.cmd_send_message.TabIndex = 9;
            this.cmd_send_message.Text = "Send Message";
            this.cmd_send_message.UseVisualStyleBackColor = true;
            this.cmd_send_message.Click += new System.EventHandler(this.cmd_send_message_Click);
            // 
            // cmd_serial_start
            // 
            this.cmd_serial_start.Location = new System.Drawing.Point(488, 95);
            this.cmd_serial_start.Name = "cmd_serial_start";
            this.cmd_serial_start.Size = new System.Drawing.Size(97, 32);
            this.cmd_serial_start.TabIndex = 10;
            this.cmd_serial_start.Text = "Start";
            this.cmd_serial_start.UseVisualStyleBackColor = true;
            this.cmd_serial_start.Click += new System.EventHandler(this.cmd_serial_start_Click);
            // 
            // cmd_serial_stop
            // 
            this.cmd_serial_stop.Location = new System.Drawing.Point(488, 145);
            this.cmd_serial_stop.Name = "cmd_serial_stop";
            this.cmd_serial_stop.Size = new System.Drawing.Size(97, 31);
            this.cmd_serial_stop.TabIndex = 11;
            this.cmd_serial_stop.Text = "Stopp";
            this.cmd_serial_stop.UseVisualStyleBackColor = true;
            this.cmd_serial_stop.Click += new System.EventHandler(this.cmd_serial_stop_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 457);
            this.Controls.Add(this.cmd_serial_stop);
            this.Controls.Add(this.cmd_serial_start);
            this.Controls.Add(this.cmd_send_message);
            this.Controls.Add(this.txt_partner);
            this.Controls.Add(this.txt_send);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_message);
            this.Controls.Add(this.textBox_Settings);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.cmd_SubForm);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmd_SubForm;
        private System.Windows.Forms.Button Exit;
        public System.Windows.Forms.TextBox textBox_Settings;
        public System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox txt_message;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_send;
        private System.Windows.Forms.TextBox txt_partner;
        private System.Windows.Forms.Button cmd_send_message;
        private System.Windows.Forms.Button cmd_serial_start;
        private System.Windows.Forms.Button cmd_serial_stop;
    }
}