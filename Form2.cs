using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace Multiformanwendung
{
    public partial class SubForm : Form
    {
        MainForm HauptForm;

        public SubForm(MainForm masterForm)
        {
            InitializeComponent();
            HauptForm = masterForm;
            comPorts();
            preSelects();
        }

        // Vorhandene COM-Ports in Listbox eintragen
        private void comPorts()
        {
            string[] portNames = SerialPort.GetPortNames();
            foreach (string s in portNames)
            {
                listBox_COM_Ports.Items.Add(s);
            }
        }

        // Parameter der seriellen Schnittstelle in Listboxen markieren
        private void preSelects()
        {
            try
            {
                listBox_COM_Ports.SetSelected(listBox_COM_Ports.FindString(HauptForm.serialPort1.PortName), true);
                listBox_Databits.SetSelected(listBox_Databits.FindStringExact(HauptForm.serialPort1.DataBits.ToString()), true);
                listBox_Baudrate.SetSelected(listBox_Baudrate.FindStringExact(HauptForm.serialPort1.BaudRate.ToString()), true);
                listBox_Parity.SetSelected(listBox_Parity.FindStringExact(HauptForm.serialPort1.Parity.ToString()), true);
                switch (HauptForm.serialPort1.StopBits.ToString())
                {
                    case "None":
                        listBox_Stopbits.SetSelected(0, true);
                        break;
                    case "One":
                        listBox_Stopbits.SetSelected(1, true);
                        break;
                    case "OnePointFive":
                        listBox_Stopbits.SetSelected(2, true);
                        break;
                    case "Two":
                        listBox_Stopbits.SetSelected(3, true);
                        break;
                }

                switch (HauptForm.serialPort1.Handshake.ToString())
                {
                    case "None":
                        listBox_Handshake.SetSelected(0, true);
                        break;
                    case "XOnXOff":
                        listBox_Handshake.SetSelected(1, true);
                        break;
                    case "RequestToSend":
                        listBox_Handshake.SetSelected(2, true);
                        break;
                    case "RequestToSendXOnXOff":
                        listBox_Handshake.SetSelected(3, true);
                        break;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmd_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmd_ok_Click(object sender, EventArgs e)
        {
            if (listBox_COM_Ports.SelectedIndex != -1)
            {
                HauptForm.serialPort1.PortName = listBox_COM_Ports.SelectedIndex.ToString();
                HauptForm.textBox_Settings.Text += "\r\n" + listBox_COM_Ports.SelectedIndex.ToString();
            }

            if (listBox_Databits.SelectedIndex != -1)
            {
                if (listBox_Databits.SelectedIndex == 0)
                    HauptForm.serialPort1.DataBits = 7;
                else
                    HauptForm.serialPort1.DataBits = 8;
                HauptForm.textBox_Settings.Text += "\r\n" + listBox_Databits.SelectedItem.ToString();
            }

            if (listBox_Stopbits.SelectedIndex != -1)
            {
                MessageBox.Show(listBox_Stopbits.SelectedIndex.ToString());
                switch (listBox_Stopbits.SelectedIndex)
                {
                    case 0:
                        HauptForm.serialPort1.StopBits = StopBits.One;
                        break;
                    case 1:
                        HauptForm.serialPort1.StopBits = StopBits.One;
                        break;
                    case 2:
                        HauptForm.serialPort1.StopBits = StopBits.OnePointFive;
                        break;
                    case 3:
                        HauptForm.serialPort1.StopBits = StopBits.Two;
                        break;
                    default:
                        MessageBox.Show(listBox_Stopbits.SelectedIndex.ToString());
                        break;
                }
                HauptForm.textBox_Settings.Text += "\r\n" + listBox_Stopbits.SelectedItem.ToString();
            }

            if (listBox_Baudrate.SelectedIndex != -1)
            {
                HauptForm.serialPort1.BaudRate = Convert.ToInt32(listBox_Baudrate.SelectedItem);
                HauptForm.textBox_Settings.Text += "\r\n" + listBox_Baudrate.SelectedItem.ToString();
            }

            if (listBox_Parity.SelectedIndex != -1)
            {
                switch (listBox_Parity.SelectedIndex)
                {
                    case 0:
                        HauptForm.serialPort1.Parity = Parity.None;
                        break;
                    case 1:
                        HauptForm.serialPort1.Parity = Parity.Even;
                        break;
                    case 2:
                        HauptForm.serialPort1.Parity = Parity.Odd;
                        break;
                }
                HauptForm.textBox_Settings.Text += "\r\n" + listBox_Parity.SelectedItem.ToString();
            }

            if (listBox_Handshake.SelectedIndex != -1)
            {
                switch (listBox_Handshake.SelectedIndex)
                {
                    case 0:
                        HauptForm.serialPort1.Handshake = Handshake.None;
                        break;
                    case 1:
                        HauptForm.serialPort1.Handshake = Handshake.XOnXOff;
                        break;
                    case 2:
                        HauptForm.serialPort1.Handshake = Handshake.RequestToSend;
                        break;
                    case 3:
                        HauptForm.serialPort1.Handshake = Handshake.RequestToSendXOnXOff;
                        break;
                }
                HauptForm.textBox_Settings.Text += "\r\n" + listBox_Handshake.SelectedItem.ToString();
            }

            Close();
        }
    }
}