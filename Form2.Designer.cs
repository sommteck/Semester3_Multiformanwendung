namespace Multiformanwendung
{
    partial class SubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_ok = new System.Windows.Forms.Button();
            this.cmd_cancel = new System.Windows.Forms.Button();
            this.listBox_COM_Ports = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.listBox_Databits = new System.Windows.Forms.ListBox();
            this.listBox_Stopbits = new System.Windows.Forms.ListBox();
            this.listBox_Baudrate = new System.Windows.Forms.ListBox();
            this.listBox_Parity = new System.Windows.Forms.ListBox();
            this.listBox_Handshake = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // cmd_ok
            // 
            this.cmd_ok.Location = new System.Drawing.Point(494, 52);
            this.cmd_ok.Name = "cmd_ok";
            this.cmd_ok.Size = new System.Drawing.Size(75, 23);
            this.cmd_ok.TabIndex = 0;
            this.cmd_ok.Text = "O.K.";
            this.cmd_ok.UseVisualStyleBackColor = true;
            this.cmd_ok.Click += new System.EventHandler(this.cmd_ok_Click);
            // 
            // cmd_cancel
            // 
            this.cmd_cancel.Location = new System.Drawing.Point(494, 113);
            this.cmd_cancel.Name = "cmd_cancel";
            this.cmd_cancel.Size = new System.Drawing.Size(75, 23);
            this.cmd_cancel.TabIndex = 2;
            this.cmd_cancel.Text = "Cancel";
            this.cmd_cancel.UseVisualStyleBackColor = true;
            this.cmd_cancel.Click += new System.EventHandler(this.cmd_cancel_Click);
            // 
            // listBox_COM_Ports
            // 
            this.listBox_COM_Ports.FormattingEnabled = true;
            this.listBox_COM_Ports.ItemHeight = 16;
            this.listBox_COM_Ports.Location = new System.Drawing.Point(38, 52);
            this.listBox_COM_Ports.Name = "listBox_COM_Ports";
            this.listBox_COM_Ports.Size = new System.Drawing.Size(120, 84);
            this.listBox_COM_Ports.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "COM-Ports:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(187, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Databits:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(329, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Stopbits:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Baudrate:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Parity:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(329, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Handshake:";
            // 
            // listBox_Databits
            // 
            this.listBox_Databits.FormattingEnabled = true;
            this.listBox_Databits.ItemHeight = 16;
            this.listBox_Databits.Items.AddRange(new object[] {
            "7",
            "8"});
            this.listBox_Databits.Location = new System.Drawing.Point(190, 52);
            this.listBox_Databits.Name = "listBox_Databits";
            this.listBox_Databits.Size = new System.Drawing.Size(120, 84);
            this.listBox_Databits.TabIndex = 10;
            // 
            // listBox_Stopbits
            // 
            this.listBox_Stopbits.FormattingEnabled = true;
            this.listBox_Stopbits.ItemHeight = 16;
            this.listBox_Stopbits.Items.AddRange(new object[] {
            "1",
            "1.5",
            "2"});
            this.listBox_Stopbits.Location = new System.Drawing.Point(332, 52);
            this.listBox_Stopbits.Name = "listBox_Stopbits";
            this.listBox_Stopbits.Size = new System.Drawing.Size(120, 84);
            this.listBox_Stopbits.TabIndex = 11;
            // 
            // listBox_Baudrate
            // 
            this.listBox_Baudrate.FormattingEnabled = true;
            this.listBox_Baudrate.ItemHeight = 16;
            this.listBox_Baudrate.Items.AddRange(new object[] {
            "300",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "74880",
            "115200"});
            this.listBox_Baudrate.Location = new System.Drawing.Point(38, 172);
            this.listBox_Baudrate.Name = "listBox_Baudrate";
            this.listBox_Baudrate.Size = new System.Drawing.Size(120, 84);
            this.listBox_Baudrate.TabIndex = 12;
            // 
            // listBox_Parity
            // 
            this.listBox_Parity.FormattingEnabled = true;
            this.listBox_Parity.ItemHeight = 16;
            this.listBox_Parity.Items.AddRange(new object[] {
            "even",
            "odd",
            "none"});
            this.listBox_Parity.Location = new System.Drawing.Point(190, 172);
            this.listBox_Parity.Name = "listBox_Parity";
            this.listBox_Parity.Size = new System.Drawing.Size(120, 84);
            this.listBox_Parity.TabIndex = 13;
            // 
            // listBox_Handshake
            // 
            this.listBox_Handshake.FormattingEnabled = true;
            this.listBox_Handshake.ItemHeight = 16;
            this.listBox_Handshake.Items.AddRange(new object[] {
            "none",
            "Xon/Xoff",
            "Hardware",
            "Software"});
            this.listBox_Handshake.Location = new System.Drawing.Point(332, 172);
            this.listBox_Handshake.Name = "listBox_Handshake";
            this.listBox_Handshake.Size = new System.Drawing.Size(120, 84);
            this.listBox_Handshake.TabIndex = 14;
            // 
            // SubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 296);
            this.Controls.Add(this.listBox_Handshake);
            this.Controls.Add(this.listBox_Parity);
            this.Controls.Add(this.listBox_Baudrate);
            this.Controls.Add(this.listBox_Stopbits);
            this.Controls.Add(this.listBox_Databits);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox_COM_Ports);
            this.Controls.Add(this.cmd_cancel);
            this.Controls.Add(this.cmd_ok);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SubForm";
            this.Text = "SubForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmd_ok;
        private System.Windows.Forms.Button cmd_cancel;
        private System.Windows.Forms.ListBox listBox_COM_Ports;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listBox_Databits;
        private System.Windows.Forms.ListBox listBox_Stopbits;
        private System.Windows.Forms.ListBox listBox_Baudrate;
        private System.Windows.Forms.ListBox listBox_Parity;
        private System.Windows.Forms.ListBox listBox_Handshake;
    }
}