using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace Multiformanwendung
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            textBox_Settings.Text = "Einstellungen:";
            textBox_Settings.Text += "\r\n" + serialPort1.PortName;
            textBox_Settings.Text += "\r\n" + serialPort1.DataBits;
            textBox_Settings.Text += "\r\n" + serialPort1.StopBits;
            textBox_Settings.Text += "\r\n" + serialPort1.BaudRate;
            textBox_Settings.Text += "\r\n" + serialPort1.Handshake;

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_SubForm_Click(object sender, EventArgs e)
        {
            SubForm SubFrm = new SubForm(this);
            SubFrm.Show();
        }

        string textText;

        private void showText(object sender, EventArgs e)
        {
            txt_partner.AppendText(textText + "\r\n");
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Hilfsvariable für eingelesenen Text deklarieren
            string message;
            // Text von serieller Schnittstelle einlesen
            // und an Hilfsvariable message ablegen
            message = serialPort1.ReadLine();
            // Text in textBox Incoming ausgeben
            txt_partner.Invoke(new EventHandler(showText));

            //txt_partner.AppendText(message + "\n");
        }

        private void cmd_send_message_Click(object sender, EventArgs e)
        {
            try
            {
                // Eingegebenen Text senden
                serialPort1.WriteLine(txt_message.Text);
                // Eingegebenen Text in TextBox Outgoing schreiben
                txt_send.AppendText(txt_message.Text + "\n");
                // TextBox Message löschen
                txt_message.Text = null;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmd_serial_start_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.Open();
                txt_send.AppendText("Serielle Schnittstelle gestartet!\n");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmd_serial_stop_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            txt_send.AppendText("Serielle Schnittstelle gestopt!\n");
        }

        // Tätigkeiten bei Programmende
        private void Serial_Communication_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Serielle Schnittstelle schließen, falls noch offen
            if (serialPort1.IsOpen)
                serialPort1.Close();
        }
    }
}
